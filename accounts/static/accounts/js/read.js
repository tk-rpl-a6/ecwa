$(document).on('click', "#delete", function () {
    if (confirm('Are you sure want to delete account?')) {
        $.ajax({
            type: 'POST',
            url: "/accounts/delete/",
            success: function (data) {
                window.location = `/accounts/login/`
            },
            Error: function(data){
                console.log("error");
            },
            fail: function(data){
                console.log("fail");
            }
        });
    }
    var json = {}
    var username = $('#username').val();
    var name = $('#name').val();

    var gender = $('#gender').find(":selected").text();

    var phone_number = $('#phone_number').val();
    var address = $('#address').val();
    var occupation = $('#occupation').val();
    var description = $('#description').val();

    json['username'] = username
    json['name'] = name
    json['gender'] = gender
    json['address'] = address
    json['occupation'] = occupation
    json['phone_number'] = phone_number
    json['description'] = description
    json = JSON.stringify(json);

    console.log(json);
    
})